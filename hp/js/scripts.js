$(document).ready(function () {

    $('.popup-iframe').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 200,
        preloader: false
    });

    $('#titleAnime').css('visibility', 'hidden');
    $(window).scroll(function () {
        setTimeout(function () {
            var windowHeight = $(window).height(),
                topWindow = $(window).scrollTop();
            $('#titleAnime').each(function () {
                var targetPosition = $(this).offset().top;
                if (topWindow > targetPosition - windowHeight + 100) {
                    $(this).addClass("fadeInUp");
                }
            });
        }, 200);
    });

    $('#infoAnime').css('visibility', 'hidden');
    $(window).scroll(function () {
        setTimeout(function () {
            var windowHeight = $(window).height(),
                topWindow = $(window).scrollTop();
            $('#infoAnime').each(function () {
                var targetPosition = $(this).offset().top;
                if (topWindow > targetPosition - windowHeight + 100) {
                    $(this).addClass("fadeInUp");
                }
            });
        }, 800);
    });

    $('#catchAnime').css('visibility', 'hidden');
    $(window).scroll(function () {
        setTimeout(function () {
            var windowHeight = $(window).height(),
                topWindow = $(window).scrollTop();
            $('#catchAnime').each(function () {
                var targetPosition = $(this).offset().top;
                if (topWindow > targetPosition - windowHeight + 100) {
                    $(this).addClass("fadeInUp");
                }
            });
        }, 500);
    });
});
